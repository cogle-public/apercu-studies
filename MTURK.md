# MTurk Notes

## Qualifications Used

| name | description |
| ---| --- |
| PARC SAR 2 Worker | Worker Participated in Phase 2 Surveys |
| PARC SAR 3 Worker | Worker Participated in Phase 3 Surveys |
| PARC SAR FSB | Worker Successfully Completed Flight School Basics Course |
| PARC SAR FSS | Worker Successfully Completed Flight School Scoring Course |
| PARC SAR P3 | Worker Completed Phase 3 Activities |


## References

https://blog.mturk.com/tutorial-best-practices-for-managing-workers-in-follow-up-surveys-or-longitudinal-studies-4d0732a7319b
