# apercu-studies

This project holds the files to build the studies used by the apercu-service.  
The studies are organized by the top level directory structure. 
The directory name corresponds to the study name the study is loaded as.

Two sample studies are present, study1 and study1.1.  Study1 has examples of how to
format the study questions.  Study1.1 is an example of how to organize a group of questions
to be included in another study.  It is included into the study1 flow.

## study.json

The study.json file in the study's directory describes the flow of the study.  The top three elements in the json are:

| field | description |
| ---| --- |
| name | name field for the study, describes the study |
| info | a structure of info about the study (see below)   |
| instruments | list of the sections/phases of the study  |
| | results are sent back to the server after completing an instrument |
| | (a browser reset restarts at the beginning of the last instrument) |

## info

This is a JSON obect with meta data about the study.  The current known meta data items are:

| field | description |
| ---| --- |
| show_progress | If this is present and set to true, a progress bar appears in the footer.  The progress bar advances for each completed instrument |
| show_score | If this is present and set to true, the score appears in the footer.  The score is updated for each completed instrument |
| popups_required | If true, popups must be enabled in the browser to run this study |

## instruments

Each instrument describes a series of 'pages', which in turn have a list of elements on them.  The user interacts with the elements (answers questions), 
and then proceeds to the next 'page'.  The important fields of an instrument are:

| field | description |
| ---| --- |
| title | This title will appear in the top banner while this instrument is active|
| name | A uniquie name for the instrument, used as a jump to location for conditional branching (optional, only needed if a branch location) |
| type | The type of this instrument (optional), for specialized instruments: 'random' |
| instruments | The list of instruments to randomize (used for 'random' instruments only)  |
| branch_logic | This is an array of conditional expressions to use as branching logic.  See below for the format.  (optional, only needed if a doing conditional branching) |
| pages | list of the individual pages show to the user  |

## 'random' instrument types

Sometimes it is desirable to randomize the path through several instruments.  To do this, the 'random' type instrument is used.  Setting an instrument's type to 'random' indicates that the instruments listed in the instruments property are traversed in a random order.  The following shows an example of using random:

```
      {
        "title":"randomize",
        "name":"r1",
        "type":"random",
        "instruments":[
          {
            "title": "Include 1st include",
            "type": "include",
            "study_dir": "T1.include"
          },
          {
            "title": "Search and Rescue Page",
            "pages": [
              {"title": "hi ",
                "elements": [
                  {"name":"T11.12", 
                   "text": "In this flight trace, the drone takes off and lands at the same airport.", 
                   "type": "multiple_choice", 
                   "choices": ["True", "False"], 
                   "required": true}
                ]
              }
            ]
          },
          {
            "title": "Include T2.include",
            "type": "include",
            "study_dir": "T2.include"
          }
        ]
      }
```

The order through the three instruments would randomly be selected each time through.

## branch_logic

This is an array of branch logic structures which allow conditional branching between instruments.  A branch logic structure has two parts, a condition, and a target.

| field | description |
| ---| --- |
| condition | This is a string which represents a python executable expression.   The questions answered in the current instrument can be queried here to perform branching based on the user responses |
| target | This is the name of the instrument to branch to if the condition evaluates to true  |

An example of a branch logic structure:

```
   "branch_logic": [
        {"condition": "'{T1.3}'=='A'", "target": "inst_1"}, 
        {"condition": "'{T1.3}'=='B'", "target": "inst_2"}]
```

In the above example, if the named element's (T1.3) answer is 'A', then the a branch to instrument named 'inst_1' is taken.  If T1.3's answer is 'B', then 'inst_2' is the next instrument, and if none of the conditions are met, the next sequential instrument is shown.

Note that the value substitution for T1.3's answer is indicated as {T1.3}.  Also, because the return value is a string, there are quotes around the {T1.3}, i.e. '{T1.3}'.  If you are doing numerical comparisons because the allowed values are numbers, you could leave off the quotes.

The question responses are substituted in the condition and evaled as a python expression.  If the result is true, the next instrument is indicated by the 'target' field. 

The branch_logic is evaluated in the order they are appear in the list.  A fixed branch condition is possible by having the condition set to "True".

#### random_path
A special value is available in branch logic called 'random_path'.  Use '{random_path}' in the branch_logic to get a random integer from 0 to 99.  The random_path value will remain the same for all of the conditions in the branch logic, and is recomputed before the next instrument's branch_logic execution.   
#### multiple selects
Note that when comparing a multiple_select type question, the answers are in array format, so your comparison should take that into account.  
So to check for having ONLY 'Football' or 'Baseball' in a multiple_select the "condition" would look like the 1st condition, while checking for either
would look like the 2nd condition

```
   "branch_logic": [
        {"condition": "{T1.4}==['Football'] or {T1.4}==['Baseball']", "target": "inst_1"}, 
        {"condition": "(True if 'Football' in {T1.4} else False) or (True if 'Baseball' in {T1.4} else False)", "target": "inst_2"}]
```
#### external app return results
External apps which are started through an html element type can return results back to apercu which can be used in both branch_logic and score_logic.  When the app url is created, the element_name parameter is set to the name of the element that holds the url.  The app can they return results back to apercu by calling the log_host url, which is also passed in the url.

The format of the call is a POST to the log_host is:

```
<log_host>/api/log/?user_id=<passed user id>&study_id=<passed study id>&element_id=app_feedback.<your app name>
```

<your_app_name> is for you to choose.  The string app_feedback. must be the 1st part of the element_id

The post data is a json string containing the named key:value pairs you want to return, along with the element_name passed into your app from the url.
Such as:

```
{"element_name": "right_side", "color": "green", "heading": 180, "horse": "trigger"}
```
#### external app return a 'done'

If the external app has the required flag of the element set, then it must return a 'done' in order to have the continue button enabled.  This is done through the same mechanism used to return values to apercu.  The app must set the property 'done' to true.

```
{element_name: "right_side", done: true}
```

**WARNING:**  Because of the asynchronous nature of the logger, try not to have multiple log calls which set external app results close together in time, or you could loose your data.  This is a current bug which is backlogged for now.


## score_logic
The score logic determines how the score is managed.  It is a python string associated with and interraction, and gets evaled when an interraction is completed.  
The score_logic property is a string representing python code that is executed 
when an interraction completes.  It's purpose is to return a new score based 
on the answers that were returned.  It uses the same formatting options for value substitution that
the branch_logic uses.  The current score is accessed using {score} in the logic string,
and must be present to add to the current score.  An example follows, which checks for a multi_select 
having a value of one item 'Chess' and a multiple_choice having 'Football' selected.  The score is incremented by 
one if the multiple_choice is correct, and by 4 if 'Football' is selected.

```angular2html
            "score_logic": "{score} + (1 if {ms1}==['Chess'] else 0) + (4 if '{mc1}'=='Football' else 0)"
```
## pages

A page describes the web content shown to the user. A page contains:


| field | description |
| ---| --- |
| title | This title will appear just below the banner on the page (can be an empty string) |
| elements | list of the individual elements/questions show on the page  |

## elements

The individual elements are what is show/asked to the user.  There are several types of elements, most of which, require a user interaction.  All elements 
have these filds in common:

| field | description |
| ---| --- |
| name | This title will appear just below the banner on the page (can be an empty string) |
| text | This is the presentation text/question of the element  |
| type | The element type  |
| required | boolean field  |
| meta_data | type dependent metadata structure  |

## overview of study.json

```
{
    "name": "Sample Study",
    "info": {},
    "instruments": [
        {
        "title": "Section 1", 
        "pages": [
            {"title": "Page 1",
                "elements": [
                    {"text": "Please enter your name.", "type": "text", "meta_data":{"label": "name"}, "required": false},
                    ...
              ]
            },
            {"title": "Page 2",
                "elements": [...]
            },
            ...
            ]
        },          
        {
        "title": "Section 2", 
        "pages": [....]
        },          
        ...
    ]
  }
```

## element types

Here is the list of element types available.

| type | description |
| ---| --- |
| para | Used to display the text field, (do not set the required flag to true) | 
| text | This depicts a single line text entry widget | 
| text_area | This is a multi-line text entry widget | 
| slider | A slider type input widget | 
| multiple_choice | A multiple choice widget | 
| multiple_select | A multiple selection widget | 
| likert | A likert scale widget | 
| tlx | A Nasa TLX widget | 
| html | An inline html widget | 
| file-html | Pulls in a local file as html | 
| pop_up | Open a new window/tab to an external site | 
| iframe_overlay | Overlay apercu with an external iframe (it must allow external iframes) with a floating 'continue' button | 
| iframe_inner | Insert an external iframe (it must allow external iframes) inside of apercu. | 
| side_window | Open an external site split screen with the questions | 

## para

The para element just contains the text to display on the screen.  It should be ascii text, not html.

An example para element:

```
{
    "text": "This is the text I would like to display.", 
    "type": "para",  
    "required": false
}
```

## text

This field is used to accept a sindle line text response from the user.  The text property of the element is displayed,
and an html input textfield is presented to accept the text response.

The text element also uses the meta_data property 'label'.  This label is placed inside the textfield to indicate the value type expected.  
For instance the meta_data.label would be 'name' for a name entry field.

A sample text element:

```
{
    "text": "How long is your driveway?", 
    "meta_data":{"label": "feet"}, 
    "type":"text", 
    "required": true
}
```

## text_area

This is similar to a text element, except that the response is multi-line.  It also used the meta_data.label field, as in the text element.

A sample text_area element:

```
{
    "text": "Can you describe the results of the experiment?", 
    "type": "text_area", 
    "meta_data":{"label": "your description"}, 
    "required": true
},
```

## slider

The slider provides an input element for a sliding scale element.  The text property of the element is displayed, followed by the slider element, and its labels.
It makes use of the meda_data field to customize the slider.  These are:

| property | description |
| ---| --- |
| min | minimum value |
| max | maximum value |
| startValue | starting value (optional) |
| labels | labels applied to the values.  A key:value object to describe the relationship between value and label |
| showValue | boolean - show the value of the slider |
| valueChar | character show after the value (such as a '%') |

Some examples of slider labels:
```
"labels":{"1":"Strongly Disagree", "2": "Disagree", "3": "Neutral", "4": "Agree", "5":"Strongly Agree"}
"labels":{"0":"Not certian", "100":"Very certain"}
```

A full slider example:

```
{
    "text": "How certian are you about your predictions?", 
    "type":"slider", 
    "meta_data": {"showValue": true, "valueChar":"%", "labels":{"0":"Not certian", "100":"Very certain"}, "startValue": 0 }
}
```

## multiple_choice

The multiple_choice elemnt provides a simple multiple choice widgen.  The extra element field used by the multiple_choice is choices.
It is a list of text values used as the choice selections.  An example might be:

```
"choices": ["Football", "Socker", "Hockey", "Baseball"]
```

A multiple_choice example:

```
{
    "text": "Which sport do you like?", 
    "type": "multiple_choice", 
    "choices": ["Football", "Socker", "Hockey", "Baseball"], 
    "required": true
},
```

## multiple_select

The multiple_select elemnt provides a simple multiple selection widget.  The extra element field used by the multiple_select is choices.
It is a list of text values used as the choice selections.  An example might be:

```
"choices": ["Clue", "Chess", "Checkers", "Blackjack", "Monopoly"]
```

A full example:

```
{
    "text": "Which games do you play?", 
    "type": "multiple_select", 
    "choices": ["Clue", "Chess", "Checkers", "Blackjack", "Monopoly"], 
    "required": true
},
```

## likert

This element provides a likert scale selection widget.  It is based on a 4 choice model of  'strongly disagree', 'disagree', 'agree', 'strongly agree', with a "Don't know, N/A" off to the side.
The "Don't know" field will be randomly placed to the left or right of the widget.

A likert example:

```
{
    "text": "Do you agree with the president?", 
    "type":"likert", 
    "meta_data": {}, 
    "required": true
},
```

## tlx

This element provides a Nasa tlx selection widget. 

A tlx example:

```
{
    "text": "How difficult was the task?", 
    "type":"tlx", 
    "meta_data": {}, 
    "required": true
},
```

## html

This element inserts the html string found in the text propery of the element into the page stream.  The html cannot access 
any of the environment, it must include all necessary external information itself.

An html example:

```
{
    "text": "<div><h3>This is an HTML header</h3></div>", 
    "type":"html", 
    "required": false
},
```

## file-html

This element inserts an external html file onto the page.  The file must exist in the study's directory.  The filename is found in the element's metadata:

| meta_data property | description |
| ---| --- |
| fileName | filename of the html file, eg: foo.html |

A file-html example:

```
{
    "name":"", "text": 
    "In this Study you will see images of a drone flying over a map, and look like in the picture below.", 
    "type": "file-html",  
    "meta_data":{"fileName": "t0-2.html"}, 
    "required": true
},
```


## pop_up

This element opens an external html page.  It will be a new window or tab, depending on the browser settings.

| meta_data property | description |
| ---| --- |
| url | full url of the site |
| tab_name | name of tab where url is to appear (optional, default is 'Popup') |
| fileName | optional study filename of url to appear in tab (use instead of url) |
| | WARNING: user_id, study_id, and study directory are in the resulting url, as with normal urls |

A pop_up example:

```
{
    "text":""
    "type": "pop_up",  
    "meta_data":{"url": "http://google.com"}, 
    "required": false
},
```


## side_window

This element opens an external html page inside the questions page. It is side by side with the questions.

| meta_data property | description |
| ---| --- |
| url | full url of the site |
| rigthSidePct | <int> percent the right side width the iframe should take |

A side_window example:

```
{
    "text":""
    "type": "side_window",  
    "meta_data":{"url": "http://google.com"}, 
    "required": false
},
```

## Note on side_window and pop_up URLs

URLs have the study_id and user_id appended to the URL using the ? and & format:
```
   url?study_id=<studyid>&user_id=<userid>&element_name=<element_name>&log_host=<logging url>
```
Additional arguments can be added to the url string by including them into the url, such as:

```
   url?arg1=<arg1 value>&arg2=<arg2 value>
```

So the final url used would be:

```
   url?study_id=<studyid>&user_id=<userid>&element_name=<element_name>&arg1=arg1_value&arg2=arg2_value
```


Note that studyid, userid, log_host and element_name are substituted for the actual values.

| param | description |
| --- | --- |
| studyid | is apercu's study_id |
| userid | is apercu's user_id |
| element_name | is the name of the element that has the url, or left out if there is no name. |
| log_host | is the url of where user logging can be sent |



An example line from the study json which calls out to a local running Klarigo is:

```
{"name": "element_url1", text": "", "type": "pop_up",  "meta_data":{"url": "http://127.0.0.1:3000?default_load=352485b9-3f3b-4ce5-9ded-b99cc8674af4&user_mode=apercu_all"}, "required": false}
```
## images

If you want to include images, use the file-html element type.  Inside the html place an image tag:

```
<div>
  <img src="../static/T1/t0.png" width="80%" />
</div>
```

The image should be in the study's directory.  In the case above that would be study "T0".

You can also store common images in the 'images' subdirectory.  It would be accessed using
the url:  "../statis/images/filename.png".

## meta_data - all elements

Any element may have the following properties in the meta_data

| property | description |
| ---| --- |
| hang_time | number - seconds to wait until the continue button is enabled. |
| | if multiple elements on a page have a hang_time, the longest one will be enforced |


## included studies

You can pull in study directories into your study as an instrument.  You must insure that the study you are pulling does not end with a response showing the completion code or last_page set to true.
To pull in a study, you would add an instrument such as:

```
    {
        "title": "Sample include", 
        "type": "include",
        "study_dir": "study1.1"
    },   
```

The study_dir indicates what study to include into your study.

## last page, completion code

Your last element should have the last_page property set to true.  It should also have the meta_data fields set as in the following element example:

```
   {
        "text": "Thank you, your survey is over.  Copy your completion code to your Turk account.", 
        "meta_data":{"label": "completion code", "completionCode":"*completion_code*", "disabled": true}, 
        "type":"text", 
        "required": true
   }

```

## uploading your study

Once you have generated your valid study.json file, and any supporting html files it is time to upload it to a running system and test it.
You do that by visiting the running system url:  such as 127.0.0.1:8000 for a local system, with the following format to upload:

```
    127.0.0.1:8000/api/import_study/?study_name=T1&load_token=3333COGLE
```

This will upload the study directory files into the running system.  You would then have to go to the database and pick out the latest study uuid to test the study.

## directory structure

The apercu-service, apercu-build, and apercu-studies must all be loaded in the same directory so that the appropriate files can be accessed.  See the container-apercu Dockerfile to see how they get loaded.



